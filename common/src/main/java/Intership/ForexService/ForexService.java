package Intership.ForexService;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

public class ForexService {
    public static MonetaryAmount calculateCurrency(Double value, String from, String to) {
        MonetaryAmount money = Monetary.getDefaultAmountFactory().setCurrency(from).setNumber(value).create();
        CurrencyConversion conversion = MonetaryConversions.getConversion(to);
        MonetaryAmount convertedAmount = money.with(conversion);
        System.out.println(convertedAmount);
        return convertedAmount;
    }
}


