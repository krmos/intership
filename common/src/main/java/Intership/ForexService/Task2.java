package Intership.ForexService;

import javax.money.MonetaryAmount;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Task2 implements Runnable {
    public static void main(String[] args) {
        System.setErr(new PrintStream(new OutputStream() {
            public void write(int b) {
            }
        }));
        Thread thread = new Thread(new Task2());
        thread.start();
    }

    public void run() {
        String from;
        String to;
        double value;
        while (true) {
            System.out.println("Валюта 1: ");

            Scanner in = new Scanner(System.in);
            from = in.next();

            System.out.println("Валюта 2: ");
            to = in.next();

            System.out.println("Значение: ");
            value = in.nextDouble();
            MonetaryAmount resultSum = null;


            resultSum = ForexService.calculateCurrency(value, from, to);

            System.out.println(resultSum.toString());
        }
    }
}
