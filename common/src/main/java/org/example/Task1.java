package org.example;

public class Task1 {
    public static void main(String[] args) {
        Person template = new Person("аvan", "Ivanov", "аvanovich", 30, "M", "1.01.1970");
        PersonsList personsList = new PersonsList();
        personsList.addPerson(new Person("аvan", "Бvanov", "Ivanovich", 20, "M", "1.01.1970"));
        personsList.addPerson(new Person("аirill", "аvanov", "Ivanovich", 16, "M", "1.01.1970"));
        personsList.addPerson(new Person("аaria", "бvanov", "Ivanovich", 19, "F","1.01.1997"));
        personsList.addPerson(new Person("бohnny", "аvanov", "Ivanovich", 11, "M", "1.01.1970"));
        personsList.addPerson(new Person("бaria", "Аvanov", "Ivanovich", 22, "F", "1.01.1997"));
        personsList.addPerson(new Person("cop", "cop", "cop", 19, "cop","cop"));
        personsList.addPerson(new Person("cop", "cop", "cop", 19, "cop","cop"));
        personsList.addPerson(new Person("cop", "cop", "cop", 19, "cop","cop"));
        personsList.addPerson(new Person("бohnny", "аvanov", "Ivanovich", 11, "M", "1.01.1970"));
        personsList.addPerson(new Person("бaria", "Аvanov", "Ivanovich", 22, "F", "1.01.1997"));
        personsList.addPerson(new Person("бohnny", "аvanov", "Ivanovich", 11, "M", "1.01.1970"));
        personsList.addPerson(new Person("бaria", "Аvanov", "Ivanovich", 22, "F", "1.01.1997"));

        if (personsList.contains(template)) {
            System.out.println("Существует");
        }
        else {
            System.out.println("Не существует");
        }

        for (int i = 0; i < personsList.size(); i++) {
            Person p = personsList.getPerson(i);
            String lName = p.getLastName().toLowerCase();
            if ((p.getAge() <= 20) &&
                    (
                            (lName.startsWith("а")
                                    || lName.startsWith("б")
                                    || lName.startsWith("в")
                                    || lName.startsWith("г")
                                    || lName.startsWith("д")
                            )
                    )
            ) {
                System.out.println(p.toString());
            }
        }

        DuplicatePersons duplicatePersons = new DuplicatePersons(personsList);

        if (duplicatePersons.getDuplicatePersons().size() < 10) {
            duplicatePersons.orderDisAge();

            for (int i = 0; i < duplicatePersons.getDuplicatePersons().size(); i++) {
                System.out.println(i + 1 + duplicatePersons.getDuplicatePersons().getPerson(i).toString());
            }
        }
        else {
            System.out.println(duplicatePersons.getDuplicatePersons());
        }


    }
}
