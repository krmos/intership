package org.example;

import javax.crypto.spec.PSource;
import java.util.Date;
import java.util.LinkedList;

public class PersonsList {

    private LinkedList<Person> persons;

    public PersonsList() {
        persons = new LinkedList<Person>();
    }

    public PersonsList(PersonsList pers) {
        persons = new LinkedList<Person>();
        for (int i = 0; i < pers.size(); i++) {
            Person temp = new Person(pers.getPerson(i).getFirstName(),
                    pers.getPerson(i).getLastName(),
                    pers.getPerson(i).getPatronymic(),
                    pers.getPerson(i).getAge(),
                    pers.getPerson(i).getSex(),
                    pers.getPerson(i).getBirthday()
            );
            persons.add(temp);
        }
    }

    public int size() {
        return persons.size();
    }

    public boolean contains(Person person) {
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).equals(person))
                return  true;
        }
        return false;
    }

    public Person getPerson(int index) {
        return persons.get(index);
    }

    public void setPerson(int index, Person pers) { persons.set(index, pers); }

    public void addPerson(Person p) {
        persons.add(p);
    }

    public void removePerson(Person p) {
        persons.remove(p);
    }

    public void removePerson(int index) {
        persons.remove(index);
    }
}
