package org.example;

import java.util.LinkedList;

public class DuplicatePersons {
    private PersonsList duplicatePersons;
    private PersonsList personsList;

    public PersonsList getDuplicatePersons() {
        return duplicatePersons;
    }

    public DuplicatePersons(PersonsList persons) {
        duplicatePersons = new PersonsList();
        personsList = persons;
        findDuplicatePerson();
    }

    private PersonsList findDuplicatePerson() {
        PersonsList personsList = new PersonsList(this.personsList);
        for (int i = 0; i < personsList.size();) {
            Person p = personsList.getPerson(0);
            personsList.removePerson(0);

            if (personsList.contains(p) && !duplicatePersons.contains(p)) {
                duplicatePersons.addPerson(p);
            }
        }
        return duplicatePersons;
    }

    public PersonsList orderDisAge() {
        for (int i = 0; i < duplicatePersons.size() - 1; i++) {
            for (int j = i; j < duplicatePersons.size(); j++) {
                if (duplicatePersons.getPerson(i).getAge() < duplicatePersons.getPerson(j).getAge()) {
                    Person temp = duplicatePersons.getPerson(i);
                    duplicatePersons.setPerson(i, duplicatePersons.getPerson(j));
                    duplicatePersons.setPerson(j, temp);
                }
            }
        }

        return duplicatePersons;
    }

    public PersonsList getDuplicatePersonsList() {
        return duplicatePersons;
    }
}
