package org.example;

import java.util.Date;

public class Person {
    private String firstName;
    private String lastName;
    private String patronymic;
    private int age;
    private String sex;
    private String birthday;

    public Person(
            String fname,
            String lName,
            String patronymic,
            int age,
            String sex,
            String birthday
    ) {
        firstName = fname;
        lastName = lName;
        this.patronymic = patronymic;
        this.age = age;
        this.sex = sex;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getAge() {
        return age;
    }

    public String getSex() {
        return sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public boolean equals(Person p) {
        if (getClass() == null || p == null)
            return false;
        return firstName.equals(p.getFirstName())
        && lastName.equals(p.getLastName())
        && patronymic.equals(p.patronymic)
        && age == p.getAge()
        && sex.equals(p.getSex())
        && birthday.equals(p.birthday);
    }

    public String toString() {
        return new String(firstName + " " + lastName + " " + patronymic + ", " + age + ", " + sex + ", " + birthday.toString());
    }
}
